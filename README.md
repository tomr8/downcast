# Downcast

This Python script downloads the "What Really Happened Feed" from the RBN network, locates the files from today's broadcast and downsamples them. The resulting files are one quarter of the size of the published 44.1 kHz files and retain signifcant clarity, making them useful for people with limited data plans.

The resampled files are then uploaded to your Fastmail account using authentication contained in your NCFTP Fastmail bookmark.

Note: It might be interesting to modify the script to upload to Google Drive using a command employing the Google Drive API.

The script is intended to be run via Linux cron job at 5 pm (PST) each day, corresponding to when the final of 3 hours have been uploaded to the RBN server. Here's your crontab entry:

0 17 * * 1-5 cd $HOME/work/wrh; python wrh.py > log.txt 2>&1

The script (wrh.py) is located in a subdirectory in your home directory. The command prints the script's output to a file named log.txt in the same directory. Errors are also redirected into the file.
