from datetime     import date
from datetime     import datetime
pass;             import feedparser
pass;             import os
pass;             import pdb    # pudb not installed on server box
from pprint       import pprint
pass;             import requests
pass;             import subprocess
pass;             import sys
pass;             import tempfile
pass;             import time
from datetime     import timedelta
from urlparse     import urlparse

debug = False

FEED_URL='http://www.republicbroadcastingarchives.org/category/michael-rivero/feed'
FEED_FILE = 'wrh_feed.rss'

# Create a visible temporary file in filesystem
# 'delete=False' so it sticks around after being closed
mktmpfile = lambda prefix, suffix: tempfile.NamedTemporaryFile(delete=False,
                                                               prefix=prefix,
                                                               suffix=suffix)
TODAY = date.today()
WEEKDAY = TODAY.strftime('%A')

def ftp_put(tmpfilename):
    """
    Remove random chars from tempfile name. Upload and remove local file.
    Input:
        tmpfilename: string: the tempfilename with random chars
        fn_ext: string: the extension of the new filename
    Output:
        None
    """
    filename = tmpfilename.split('_')[:-1]   # random chars always last element
    ext = os.path.splitext(tmpfilename)[1]
    filename = '_'.join(filename) + ext
    os.rename(tmpfilename, filename)
    # $> ncftpput [flags] bookmark-name remote-dir local-files...
    # Suppress ncftp UI with -V
    subprocess.call(['ncftpput',
                     '-V',
                     'fastmail',
                     'public/wrh',
                     filename])
    os.unlink(filename)

def process_audiofile(entry, link, hour):
    """
    Resamples a full-length audio file.
    Uploads the resampled file to the server.
    Input:
        entry: text: Item of RSS feed
        link: text: URL of published file (rbn.com/...)
    """
    print "Processing %s..." % entry.title

    # TODO: Upload actual filename
    filepath = urlparse(link).path
    filename = os.path.basename(filepath)

    print "Fetching %s..." % filename
    response = requests.get(link, stream=True)

    prefix='%02d_%02d_%d_' % (now.month,
                              now.day,
                              hour)

    # Write mp3 file
    audioinfile = mktmpfile(prefix, suffix='.mp3')
    filesize = 0
    # Note: This *would* download in chunks if streaming
    # was enabled on the server. Since it isn't, the file
    # is simply downloaded into 28 MB memory BLOB
    for chunk in response.iter_content(chunk_size=1024):
        if chunk:   # filter out keep-alive new chunks
            audioinfile.file.write(chunk)
            filesize += 1024
    # Alternate method
#   audio = response.content    # binary data
#   audioinfile.file.write(audio)
    audioinfile.file.close()

    response.close()    # Close the connection

    print "Fetched %d bytes in %s" % (filesize, filename)

    audiooutfile = mktmpfile(prefix, suffix='.mp3')
    audiooutfile.file.close() # Never needs to be opened

    lame = ['lame',
            '--quiet',
#           '--verbose',
            '--resample',
            '11.025',
            '-b',
            '16',
            audioinfile.name,
            audiooutfile.name]

    sox = ['sox',
           audioinfile.name,
           audiooutfile.name,
           'rate',
           '11025']

    print "Resampling file ", audioinfile.name
    # Sox appears to behave more reliably
    subprocess.call(sox)

    # Temp files must be explicitly removed
    os.unlink(audioinfile.name)

    print "Uploading resampled file ", audiooutfile.name
    # $> ncftpput [flags] bookmark-name remote-dir local-files...
    # Suppress ncftp UI with -V

    ftp_put(audiooutfile.name)

def process_htmlfile(links):
    """
    Construct HTML from links and upload with today's date
    Input:
        links: list of URLs
    """
    if len(links) > 0:
        # At least one file has been published for today
        html = """
<html>
    <body>
        Links to <a href=%s>WRH files</a> for %s, %s:
""" % (FEED_URL, WEEKDAY, TODAY)

        links.reverse()
        i = 1

        # Add links to HTML
        for link in links:
            # Strip all but hour from RBN's title for the show,
            # e.g.: ".../stream_2018-06-22_165936.mp3" becomes "16"
            html += """
<br><a href=\"%s\">Hour %s</a>
            """ % (link, i)
            i += 1
    else:
        # No files published for today
        html = """
<html>
    <body>
        No <a href=%s>WRH files</a> were found for %s, %s.
""" % (FEED_URL, WEEKDAY, TODAY)

    # Close the HTML doc
    html += "</body></html>"

    # Create empty HTML tempfile
    # Name tempfile today's date (e.g. 18_06_21_jKz3leF.html)
    #
    prefix='%02d_%02d_' % (now.month, now.day)
    htmlfile = mktmpfile(prefix, suffix='.html')
    htmlfile.file.write(html)
    htmlfile.file.close()

    ftp_put(htmlfile.name)

if __name__ == '__main__':

    # Read the test feed (for testing)
#   with open(FEED_FILE, 'r') as f:
#       source = f.read()
    print "Retrieving WRH feed..."
    source = requests.get(FEED_URL).text.encode('utf-8')

    # Write the test feed (for testing)
#   with open(FEED_FILE, 'wb') as f:
#       f.write(source)
#       f.close()

    # Use feedparser to parse feed XML
    entries = feedparser.parse(source).entries

    # TODO: correct for daylight savings
    # TODO: use tz_info
    # correct for UTC
    now = datetime.now()
    # testing (after a day has passed since publishing)
#   now = now - timedelta(days=1)
    utc = now + timedelta(hours=7)

#   There are many more than 3 entries the way this is coded
#   # TODO: restrict number of located entries?
#   if len(entries) != 3:
#       # One or more files not available
#       sys.exit(0)

    links = []

    print "Processing %d RSS items..." % len(entries)
    for entry in entries:

        if "What Really Happened" not in entry.title:
            continue
        try:
            # There *should* be only one link...
            link = entry.enclosures[0]['href']
        except AttributeError:
            print "Entry has no enclosures: ", entry
            continue
        except IndexError:
            print "No enclosure in", entry.enclosures
            continue
        except KeyError:
            print "No valid link for show. Skipping:", entry.enclosures
            continue

        # Began returning incorrect day (+1) when DST started
#       published = datetime.fromtimestamp(
#               # updated_parsed is a time.time_struct
#               time.mktime(entry.published_parsed))

        # This is UTC
        pp = entry.published_parsed
        published = datetime.strptime("%d_%d_%d_%d_%d" % (pp.tm_year,
                                                          pp.tm_mon,
                                                          pp.tm_mday,
                                                          pp.tm_hour,
                                                          pp.tm_min),
                                      "%Y_%m_%d_%H_%M")

        # TODO: rewrite to skip (continue) if > timedelta
        # Allow a few hours to prevent overlap
        if utc - published < timedelta(hours=18):
            # Item published in last few hours

            print "Adding link for %s..." % entry.title
            links.append(link)

            # TODO: use regex here
            if 'Hour 1' in entry.title:
                hour = 1
            elif 'Hour 2' in entry.title:
                hour = 2
            elif 'Hour 3' in entry.title:
                hour = 3
            else:
                # Hour not found in title
                # This really shouldn't happen, but raising an exception
                # might prevent other valid files from being processed
                continue

            if not debug:
                # Comment this out when not resampling
#               process_audiofile(entry, link, hour)
                pass
            else:
                print "Processing file: "
                print link, hour

    if not debug:
        process_htmlfile(links)
    else:
        print links
